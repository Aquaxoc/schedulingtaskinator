import subprocess


class PerfModel:
    def __init__(self, hostname: str, perf_model_dir: str = "/usr/local/share/starpu/perfmodels/sampling/"):
        self.hostname = hostname
        self.path = perf_model_dir
        self.model = {}

    def _search_in_model(self, codelet: str, hash: str) -> float:
        grep = subprocess.Popen([
            "/bin/sh",
            "-c",
            f'grep -e "{hash}" {self.path}/codelets/*/*{codelet.lower()}*{self.hostname} '
        ], stdout=subprocess.PIPE)
        vals = []
        for line in grep.stdout:
            vals.append(float(line.split(b"\t")[3].rstrip())/1000.0)  # (mean) µs -> ms
        grep.stdout.close()
        res = min(vals)
        self.model[(codelet, hash)] = res
        return res

    def get_best_time(self, codelet: str, hash: str) -> float:
        if (codelet, hash) in self.model:
            return self.model[(codelet, hash)]
        else:
            return self._search_in_model(codelet, hash)
