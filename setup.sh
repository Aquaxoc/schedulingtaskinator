#!/bin/bash

echo "Creating venv and installing requirements..."
# Setup venv
python3 -m venv venv
# Install requirements
venv/bin/python3 -m pip install -r requirements.txt
echo "Done"