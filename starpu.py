import os
import subprocess
from static_sched import StaticSched


class StarPUReplay:
    def __init__(self, task_graph: str, export_dir: str, scheduler: str, perf_model_dir: str, hostname: str):
        self._initial_task_graph_path = task_graph
        self._export_dir = export_dir
        self._scheduler = scheduler
        self._perf_model_dir = perf_model_dir
        self._hostname = hostname
        self._env = os.environ.copy()

    def run(self, static_sched: StaticSched = None):
        """Run starpu_replay"""
        self._env.update(
            STARPU_GENERATE_TRACE="1",
            STARPU_FXT_TRACE="1",
            STARPU_FXT_PREFIX=self._export_dir,
            STARPU_SCHED=self._scheduler,
            STARPU_PERF_MODEL_DIR=self._perf_model_dir,
            STARPU_HOSTNAME=self._hostname
        )
        command = ["starpu_replay", self._initial_task_graph_path]
        if static_sched is not None:
            static_sched.export_file(self._export_dir + "/sched.rec")
            command.append(self._export_dir + "/sched.rec")
        subprocess.call(command, env=self._env)
