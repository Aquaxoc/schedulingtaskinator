from __future__ import annotations
import subprocess
import pandas as pd
import numpy as np
from static_sched import StaticSched
from starpu import StarPUReplay
from perf_model import PerfModel
from slack import Graph


class Simu:
    df_tasks: pd.DataFrame
    df_app: pd.DataFrame
    df_deps: pd.DataFrame
    workers: dict[int, str]
    graph: Graph
    static_sched: StaticSched

    def __init__(self,
                 task_graph: str,
                 export_dir: str = None,
                 hostname: str = "mirage",
                 scheduler: str = "dmdas",
                 perf_model_dir: str = "/usr/local/share/starpu/perfmodels/sampling"):
        self._export_dir = "/tmp/schedulingtaskinator" if export_dir is None else export_dir
        self.gantt_data = []
        self._starpu_backend = StarPUReplay(
            task_graph=task_graph,
            export_dir=self._export_dir,
            scheduler=scheduler,
            perf_model_dir=perf_model_dir,
            hostname=hostname
        )
        self.static_sched = StaticSched()
        self.model = PerfModel(hostname, perf_model_dir)
        self.run()

    def run(self):
        self._starpu_backend.run(self.static_sched)
        self._get_data()
        self._prepare_data()

    def _get_data(self):
        """Get data from starpu files and format it"""
        tasks_file = open(self._export_dir+"/tasks.csv", "w")
        subprocess.call(["rec2csv", self._export_dir + "/tasks.rec"], cwd=self._export_dir, stdout=tasks_file)
        tasks_file.close()
        self.df_tasks = pd.read_csv(self._export_dir + "/tasks.csv")

        # save last static_sched constraints list
        self.static_sched.save_constraints(self.df_tasks["EndTime"].max())

        # get all task that are not from starpu
        self.df_app = self.df_tasks[~pd.isnull(self.df_tasks["StartTime"])]

        # get tasks dependencies
        grep = subprocess.Popen(["grep", "-E", '\t "task_[0-9]+"->"task_[0-9]+"', self._export_dir+"/dag.dot"],
                                stdout=subprocess.PIPE,
                                encoding="utf-8")
        uniq = subprocess.Popen(["uniq"], stdin=grep.stdout, stdout=subprocess.PIPE, encoding='utf-8')
        sed = subprocess.Popen(["sed", "-r", 's/\t "task_([0-9]+)"->"task_([0-9]+)"/\\2,\\1/g'],
                               stdin=uniq.stdout,
                               stdout=subprocess.PIPE,
                               encoding="utf-8")
        self.df_deps = pd.read_csv(sed.stdout, names=["JobId", "DependsOn"])
        grep.stdout.close()
        uniq.stdout.close()
        sed.stdout.close()

        # get resources names & id
        self.workers = {}
        grep = subprocess.Popen(["grep", "-E", "name\t[0-9]+\t", self._export_dir+"/activity.data"],
                                stdout=subprocess.PIPE)
        for line in grep.stdout:
            vals = line.split(b'\t')
            id = int(vals[1])
            name = vals[2].decode().rstrip()
            self.workers[id] = name
        grep.stdout.close()

    def _prepare_data(self):
        self.graph = Graph(self.df_app, self.df_deps, self.model)
        self.graph.compute_slack()
        self.gantt_data.clear()
        for jobType in self.df_app["Name"].unique():
            dt = self.df_app[self.df_app["Name"] == jobType]
            customdata = dt.loc[:, ["Name", "JobId", "SubmitOrder", "StartTime", "EndTime", "Priority", "Tag"]]
            customdata = customdata.merge(self.graph.get_slack_df(), how="inner", on="SubmitOrder")
            self.gantt_data.append({
                "name": jobType,
                "base": dt["StartTime"],
                "x": dt["EndTime"] - dt["StartTime"],
                "y": dt["WorkerId"],
                "customdata": customdata,
                "pattern": np.where(dt["SubmitOrder"].isin(self.static_sched.get_constrained_tasks_submit_order()),
                                    "x", "")
            })

    def get_worker_name(self, workerid: int):
        return self.workers[workerid]

    def get_parents(self, jobid):
        parents = self.df_deps[self.df_deps["JobId"] == jobid]
        parents = pd.merge(parents["DependsOn"], self.df_app, left_on="DependsOn", right_on="JobId", how="left")
        return parents[~parents["StartTime"].isnull()]

    def get_children(self, jobid):
        children = self.df_deps[self.df_deps["DependsOn"] == jobid]
        children = pd.merge(children["JobId"], self.df_app, on="JobId", how="left")
        return children[~children["StartTime"].isnull()]

    def get_constraint_parents(self, submitorder):
        parents = pd.DataFrame(data=self.static_sched.get_dependency_constrains_of_task(submitorder))
        parents = pd.merge(parents, self.df_app, left_on="DependsOn", right_on="SubmitOrder", how="left")
        return parents[~parents["StartTime"].isnull()]

    def get_current_execution_time(self):
        return self.static_sched.get_last_exec_time()
