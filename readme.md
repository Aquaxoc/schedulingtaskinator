# Scheduling Taskinator
Play with StarPU task graph and alter scheduling.

# Requirements
- Python 3.8 (using pip and venv is recommended) 
- RecUtils
- SimGrid
- FxT Tools
- StarPU with SimGrid and FXT support enabled.

# Installation
## Dependencies
On a debian machine, first install the dependencies:
```shell
apt install python3-pip python3-venv recutils libfxt-dev libsimgrid-dev
```

## StarPU
Then, [clone from gitlab](https://gitlab.inria.fr/starpu/starpu/) or
[download the latest version of StarPU](https://files.inria.fr/starpu/) and compile it with simgrid support enabled.

```shell
./autogen.sh  # git version only

mkdir build && cd build
../configure --with-fxt --enable-simgrid --disable-build-examples  --disable-build-tests
make -j12
    
make install 
```

Alternatively, you can change the default installation folder with the `PKG_CONFIG_PATH` environment variable
for the `configure` script, but `starpu_replay` must be in the path for the execution.

## Python virtual environment setup (venv)
Just run `./setup.sh` to set up the virtual environment and install the requirements.

# Usage
```commandline
usage: main.py [-h] [-s SCHEDULER] [-H HOST] [-m PERF_MODEL_DIR] tasks.rec

Modify StarPU scheduling visually

positional arguments:
  tasks.rec             Task graph provided by StarPU

options:
  -h, --help            show this help message and exit
  -s SCHEDULER, --scheduler SCHEDULER
                        Name of the scheduler (default: dmdas)
  -H HOST, --host HOST, --hostname HOST
                        Hostname of the simulated machine (default: mirage)
  -m PERF_MODEL_DIR, --perf-model-dir PERF_MODEL_DIR
                        StarPU perf models directory (default: /usr/local/share/starpu/perfmodels/sampling)
```

Several task graphs are given in the `examples` folder. Each `tasks_Y.rec` is a Cholesky factorisation task graph of
a Y×Y tiled matrix

Either run `python main.py examples/tasks_10.rec` in the venv,
or simply run `./run.sh examples/tasks_10.rec` to do it automatically.

Open your favourite web browser and go to http://127.0.0.1:8050.

![tuto.png](tuto.png)

Below the trace you have 3 columns:
- On the left, you have some information on the selected task. (note that you will get more info by hovering a task)
- In the middle, you can edit or remove the constraints that you added before
- On the right, you can rollback to previous states
