from __future__ import annotations
import os
import copy


class Constraint:
    """Constraint abstract class for static scheduling

    Attributes
    ----------
    submit_order: int
        Submit order of the task being constrained

    Methods
    -------
    edit(changes: dict) -> None
        Edits the constraint
    """

    submit_order: int

    def __init__(self, submit_order: int):
        self.submit_order = submit_order

    def __str__(self) -> str:
        """Returns a string to write in the static sched file, without \n at the end"""
        pass

    def edit(self, **changes):
        """Edits the constraint with a dictionary"""
        pass


class WorkerConstraint(Constraint):
    """Worker constraint

    Attributes
    ----------
    submit_order: int
        Submit order of the task being constrained
    workerid: int
        ID of the worker
    """

    workerid: int

    def __init__(self, submit_order: int, workerid: int):
        super().__init__(submit_order)
        self.workerid = workerid

    def __str__(self) -> str:
        return f"SpecificWorker: {self.workerid}"

    def edit(self, **changes):
        """
        Parameters
        ----------
        changes:
            worker_type, worker_num
        """
        if e := changes.get("workerid"):
            self.workerid = e


class DependencyConstraint(Constraint):
    """Dependency constraint

    Attributes
    ----------
    submit_order: int
        Submit order of the task being constrained
    parent: int
        Submit order of the new parent
    """

    parent: int

    def __init__(self, submit_order: int, parent: int,):
        super().__init__(submit_order)
        self.parent = parent

    def __str__(self) -> str:
        return f"DependsOn: {self.parent}"

    def edit(self, **changes):
        """
        Parameters
        ----------
        changes:
            parent
        """
        if e := changes.get("parent"):
            self.parent = e


class PriorityConstraint(Constraint):
    """Priority constraint

    Attributes
    ----------
    submit_order: int
        Submit order of the task being constrained
    priority: int
        New priority of the task
    """

    priority: int

    def __init__(self, submit_order: int, priority: int, ):
        super().__init__(submit_order)
        self.priority = priority

    def __str__(self) -> str:
        return f"Priority: {self.priority}"

    def edit(self, **changes):
        """
        Parameters
        ----------
        changes:
            priority
        """
        if e := changes.get("priority"):
            self.priority = e


class StaticSched:
    _constraints: dict[int, Constraint] = {}
    _i: int = -1  # internal index
    history: list[dict] = []  # [{"sched": constraints, "time": 1234.5678}]

    @property
    def constraints(self):
        return self._constraints

    def add_constraint(self, constraint: Constraint) -> int:
        """Adds a constraint

        Returns:
            internal id of the added constraint
        """
        self._i += 1
        self._constraints[self._i] = constraint
        return self._i

    def edit_constraint(self, index, **changes):
        if self.get_constraint(index) is not None:
            self._constraints[index].edit(**changes)

    def get_constraint(self, index):
        return self._constraints.get(index)

    def remove_constraint(self, index):
        self._constraints.pop(index)

    def get_constrained_tasks_submit_order(self):
        return [x.submit_order for x in self._constraints.values()]

    def get_dependency_constrains_of_task(self, submit_order) -> dict:
        return {"SubmitOrder": [x.submit_order for x in self._constraints.values()
                                if isinstance(x, DependencyConstraint) and x.submit_order == submit_order],
                "DependsOn": [x.parent for x in self._constraints.values()
                              if isinstance(x, DependencyConstraint) and x.submit_order == submit_order]}

    def export_file(self, filename: str):
        # group constraints by submit_order
        constraints = {}
        for _, c in self._constraints.items():
            if c.submit_order not in constraints:
                constraints[c.submit_order] = []
            constraints[c.submit_order].append(c)
        os.makedirs(os.path.dirname(filename), mode=0o700, exist_ok=True)
        file = open(filename, "w")
        for submit_order, cs in constraints.items():
            file.write(f"SubmitOrder: {submit_order}\n")
            deps = []
            for c in cs:
                if isinstance(c, DependencyConstraint):
                    deps.append(c.parent)
                else:
                    file.write(f"{c}\n")
            # special DependencyConstraint treatment
            if len(deps) > 0:
                file.write(f"DependsOn:{''.join([f' {e}' for e in deps])}")
            file.write('\n')
        file.close()

    def save_constraints(self, time: float):
        self.history.append({"sched": copy.deepcopy(self._constraints), "time": time})

    def get_last_exec_time(self) -> float:
        return self.history[-1]["time"]

    def rollback(self, index):
        self._constraints = copy.deepcopy(self.history[index]["sched"])
